﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using AarhusWebDevCoop.ViewModels;
using System.Net.Mail;
using Umbraco.Core.Models;


namespace AarhusWebDevCoop.Controllers
{
    public class ContactFormSurfaceController : SurfaceController
    {
        /// <summary>
        /// Metode der returnerer PartialView Contactform
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //returnerer partielview contatcform
            return PartialView("ContactForm", new ContactForm());
        }

        /// <summary>
        /// Metode der bliver kørt når der trykkes på submit knappen på kontakt formen metoden bliver
        /// defineret i partialview filen
        /// </summary>
        /// <param name="model">objekt af typen ContactForm med indholdet fra felterne i formen</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult HandleFormSubmit(ContactForm model)
        {
            //hvis alle required felter ikke er sat returneres til nuværende side
            if (!ModelState.IsValid) { return CurrentUmbracoPage(); }

            //her oprettes et mailmessage object
            MailMessage message = new MailMessage();
            //modtager at beskeden bliver sat
            message.To.Add("npbhdk@gmail.com");
            //subject af beskeden bliver sat fra model objektet
            message.Subject = model.Subject;
            //afsender af beskeden bliver sat fra model objektet
            message.From = new MailAddress(model.Email, model.Name);
            //body bliver sat på beskeden fra model objektet
            message.Body = model.Message;


            //opretter smtp client og sender mail beskeden
            using (SmtpClient smtp = new SmtpClient())
            {
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential("npbhdk@gmail.com", "jfajxhzcmffgnbtz");

                // send mail
                smtp.Send(message);
                //sætter tempdata variblen success til true
                TempData["success"] = true;
            }

            //opretter en en besked ved at bruger contentservice api'en og sætter værdierne fra contact form model objektet
            IContent msg = Services.ContentService.CreateContent(model.Subject, CurrentPage.Id, "message");
            msg.SetValue("messageName", model.Name);
            msg.SetValue("email", model.Email);
            msg.SetValue("subject", model.Subject);
            msg.SetValue("messageContent", model.Message);

            //gemmer beskeden
            Services.ContentService.Save(msg);

            //redirecter til nuværende umbraco side, da TempData["success"] er sat til true bliver formen ikke vist, der bliver 
            //istedet vist en besked om at beskeden er blevet sendt.
            return RedirectToCurrentUmbracoPage();
        }
    }
}